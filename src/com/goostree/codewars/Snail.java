package com.goostree.codewars;

public class Snail {
	public static void main(String[] args) {
      int[][] arr = { {1, 2, 3},
                      {8, 9, 4},
                      {7, 6, 5}};

      int[][] arr2 = {{1, 2, 3, 4},
                      {12, 13, 14, 5},
                      {11, 16, 15, 6},
                      {10, 9, 8, 7}};   
      
      int [][] arr3 = {{1}};
      
      int [][]arr4 = {{}};

      int[] items = snail(arr4);
      for(int item : items) {
    	  System.out.print(item + " ");
      }

    }

    public static int[]  snail(int[][] items) {
        return spiral(items, 0, items[0].length, items.length, 0);
    }
    
    private static int[] spiral(int[][]items, int top, int right, int bottom, int left) {
        
        //right and bottom are the size in each dimension - though they're the same 
        //for square arrays. element count is their product.
        int[] results = new int[right*bottom];
        int index = 0;
        
        while(top < bottom && left < right) {
        	int currX = left;
            int currY = top;
        	
	        //top row
	        while(currX < right) {
	            results[index] = items[currY][currX];
	            index++;
	            currX++;
	        }
	        currY++;
	        currX--;
	        top++;
	
	        //right column
	        while(currY < bottom) {
	        	results[index] = items[currY][currX];
	        	index++;
	            currY++;
	        }
	        currY--;
	        currX--;
	        right--;
	
	        //bottom row
	        while(currX >= left) {
	        	results[index] = items[currY][currX];
	        	index++;
	            currX--;
	        }
	        currX++;
	        currY--;
	        bottom--;
	
	        //left column
	        while(currY >= top) {
	        	results[index] = items[currY][currX];
	        	index++;
	            currY--;
	        }
	        left++;
        }
        
        return results;
    }
}
